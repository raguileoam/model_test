package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Periodo {

    @Id
    @GeneratedValue
    private Long perId;
    private Date perInicio;
    private Date perTermino;
    private int perPeriodo;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "per_id")
    private List<Horario> horarios = new ArrayList<>();

    public Periodo() {
    }

    public Periodo(Date perInicio, Date perTermino, int perPeriodo) {
        this.perInicio = perInicio;
        this.perTermino = perTermino;
        this.perPeriodo = perPeriodo;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public Date getPerInicio() {
        return perInicio;
    }

    public void setPerInicio(Date perInicio) {
        this.perInicio = perInicio;
    }

    public Date getPerTermino() {
        return perTermino;
    }

    public void setPerTermino(Date perTermino) {
        this.perTermino = perTermino;
    }

    public int getPerPeriodo() {
        return perPeriodo;
    }

    public void setPerPeriodo(int perPeriodo) {
        this.perPeriodo = perPeriodo;
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public void setHorario(List<Horario> horarios) {
        this.horarios = horarios;
    }
}