package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class DetalleAsignatura {

    @Id
    @GeneratedValue
    private Long dasId;
    private String dasNombre;
    private String dasCodigo;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "das_id")
    private List<Asignatura> asignaturas = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "das_id")
    private List<Horario> horarios = new ArrayList<>();

    public DetalleAsignatura() {
    }

    public Long getDasId() {
        return dasId;
    }

    public void setDasId(Long dasId) {
        this.dasId = dasId;
    }

    public String getDasNombre() {
        return dasNombre;
    }

    public void setDasNombre(String dasNombre) {
        this.dasNombre = dasNombre;
    }

    public String getDasCodigo() {
        return dasCodigo;
    }

    public void setDasCodigo(String dasCodigo) {
        this.dasCodigo = dasCodigo;
    }

    public List<Horario> getHorario() {
        return horarios;
    }

    public void setHorario(List<Horario> horario) {
        this.horarios = horario;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<Horario> horarios) {
        this.horarios = horarios;
    }

    public DetalleAsignatura(String dasNombre, String dasCodigo) {
        this.dasNombre = dasNombre;
        this.dasCodigo = dasCodigo;
    }
}