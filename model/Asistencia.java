package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Asistencia {

    @Id
    @GeneratedValue
    private Long asiId;
    private String asiCodigo;
    private Date asiHora;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cla_id")
    private Clase clase;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "aes_id")
    private AsistenciaEstado asistenciaEstado;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ins_id")
    private Inscripcion inscripcion;

    public Asistencia() {
    }

    public Long getAsiId() {
        return asiId;
    }

    public void setAsiId(Long asiId) {
        this.asiId = asiId;
    }

    public String getAsiCodigo() {
        return asiCodigo;
    }

    public void setAsiCodigo(String asiCodigo) {
        this.asiCodigo = asiCodigo;
    }

    public Date getAsiHora() {
        return asiHora;
    }

    public void setAsiHora(Date asiHora) {
        this.asiHora = asiHora;
    }

    public Clase getClase() {
        return clase;
    }

    public void setClase(Clase clase) {
        this.clase = clase;
    }

    public AsistenciaEstado getAsistenciaEstado() {
        return asistenciaEstado;
    }

    public void setAsistenciaEstado(AsistenciaEstado asistenciaEstado) {
        this.asistenciaEstado = asistenciaEstado;
    }

    public Inscripcion getInscripcion() {
        return inscripcion;
    }

    public void setInscripcion(Inscripcion inscripcion) {
        this.inscripcion = inscripcion;
    }

    public Asistencia(Clase clase, AsistenciaEstado asistenciaEstado, Inscripcion inscripcion) {

        this.clase = clase;
        this.asistenciaEstado = asistenciaEstado;
        this.inscripcion = inscripcion;
    }

    @Override
    public String toString() {
        return "Asistencia{" +
                "id=" + asiId +
                ", hora de asistencia=" + asiHora +
                ", asistenciaEstado=" + asistenciaEstado.getAesNombre() +
                '}';
    }
}