package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;

@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long usuId;

    private String usuClave;

    private Boolean usuActivo;

    @Column(unique = true)
    private String usuEmail;

    @OneToOne
    @MapsId
    private Rol rol;

    public Usuario() {
        this.rol = new Rol();
    }

    public Usuario(String clave, Boolean activo, String email) {
        this.usuClave = clave;
        this.usuActivo = activo;
        this.rol = new Rol();
        this.usuEmail = email;
    }

    public Long getUsuId() {
        return usuId;
    }

    public void setUsuId(Long usuId) {
        this.usuId = usuId;
    }

    public String getUsuClave() {
        return usuClave;
    }

    public void setUsuClave(String usuClave) {
        this.usuClave = usuClave;
    }

    public Boolean getUsuActivo() {
        return usuActivo;
    }

    public void setUsuActivo(Boolean usuActivo) {
        this.usuActivo = usuActivo;
    }

    public String getUsuEmail() {
        return usuEmail;
    }

    public void setUsuEmail(String usuEmail) {
        this.usuEmail = usuEmail;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
}
