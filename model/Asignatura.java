package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Asignatura {

    @Id
    @GeneratedValue
    private Long asgId;

    @Column(name = "asg_retraso")
    private Integer asgRetraso;

    @ManyToOne
    @JoinColumn(name = "das_id")
    private DetalleAsignatura detalle;

    @ManyToOne
    @JoinColumn(name = "pro_rut")
    private Profesor profesor;

    @ManyToOne
    @JoinColumn(name = "sem_id")
    private Semestre semestre;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "asg_id")
    private List<Clase> clases = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "asg_id")
    private List<Inscripcion> inscripciones = new ArrayList<>();

    public Asignatura() {
    }

    public Long getAsgId() {
        return asgId;
    }

    public void setAsgId(Long asgId) {
        this.asgId = asgId;
    }

    public Integer getAsgRetraso() {
        return asgRetraso;
    }

    public void setAsgRetraso(Integer asgRetraso) {
        this.asgRetraso = asgRetraso;
    }

    public DetalleAsignatura getDetalle() {
        return detalle;
    }

    public void setDetalle(DetalleAsignatura detalle) {
        this.detalle = detalle;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public List<Clase> getClases() {
        return clases;
    }

    public void setClase(List<Clase> clases) {
        this.clases = clases;
    }

    public List<Inscripcion> getInscripciones() {
        return inscripciones;
    }

    public void setInscripciones(List<Inscripcion> inscripciones) {
        this.inscripciones = inscripciones;
    }

    public Asignatura(Integer retraso, DetalleAsignatura detalle, Profesor profesor, Semestre semestre) {
        this.asgRetraso = retraso;
        this.detalle = detalle;
        this.profesor = profesor;
        this.semestre = semestre;
    }

    public boolean esId(long id_2) {
        if (this.asgId == id_2) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Asignatura{" +
                "asgId=" + asgId +
                ", semestre=" + semestre +
                '}';
    }
}