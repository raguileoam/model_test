package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "estudiante")
public class Estudiante {

    @Id
    @Column(name = "est_rut")

    private Long estRut;

    @Column(name = "est_dv")
    private String estDv;

    @Column(name = "est_nombres")
    private String estNombres;

    @Column(name = "est_apellidos")
    private String estApellidos;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "est_rut")
    private List<Matricula> matriculas = new ArrayList<>();

    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(nullable = false, name = "usu_id")
    private Usuario usuario;

    public Estudiante() {
        this.usuario = new Usuario();
        this.matriculas = new ArrayList<>();
    }

    public Estudiante(Long estRut, String estApellidos, String estDv, String estNombres, Usuario usuario) {
        this.estRut = estRut;
        this.estApellidos = estApellidos;
        this.estDv = estDv;
        this.estNombres = estNombres;
        this.usuario = usuario;
        this.matriculas = new ArrayList<>();
    }

    public Long getEstRut() {
        return estRut;
    }

    public void setEstRut(Long estRut) {
        this.estRut = estRut;
    }

    public String getEstDv() {
        return estDv;
    }

    public void setEstDv(String estDv) {
        this.estDv = estDv;
    }

    public String getEstNombres() {
        return estNombres;
    }

    public void setEstNombres(String estNombres) {
        this.estNombres = estNombres;
    }

    public String getEstApellidos() {
        return estApellidos;
    }

    public void setEstApellidos(String estApellidos) {
        this.estApellidos = estApellidos;
    }

    public List<Matricula> getMatriculas() {
        return matriculas;
    }

    public void setMatriculas(List<Matricula> matriculas) {
        this.matriculas = matriculas;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
