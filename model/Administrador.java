package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;

@Entity
public class Administrador {

    @Id
    private Long admRut;

    private String admApellidos;

    private String admDv;

    private String admNombres;

    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(nullable = false, name = "usu_id")
    private Usuario usuario;

    public Administrador() {
        this.usuario = new Usuario();
    }

    public Administrador(Long admRut, String admApellidos, String admDv, String admNombres, Usuario usuario) {
        this.admRut = admRut;
        this.admApellidos = admApellidos;
        this.admDv = admDv;
        this.admNombres = admNombres;
        this.usuario = usuario;
    }

    public Long getAdmRut() {
        return admRut;
    }

    public void setAdmRut(Long admRut) {
        this.admRut = admRut;
    }

    public String getAdmApellidos() {
        return admApellidos;
    }

    public void setAdmApellidos(String admApellidos) {
        this.admApellidos = admApellidos;
    }

    public String getAdmDv() {
        return admDv;
    }

    public void setAdmDv(String admDv) {
        this.admDv = admDv;
    }

    public String getAdmNombres() {
        return admNombres;
    }

    public void setAdmNombres(String admNombres) {
        this.admNombres = admNombres;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
