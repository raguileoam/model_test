package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.Matricula;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface MatriculaDao extends CrudRepository<Matricula, Long> {

    Matricula findBymatNumero(String numero);

}
