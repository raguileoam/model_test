package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.Estudiante;
import cl.ufro.dci.sra346servicios.model.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface EstudianteDao extends CrudRepository<Estudiante, Long> {
    Estudiante findByUsuario(Usuario usuario);
}
