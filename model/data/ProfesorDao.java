package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.Profesor;
import cl.ufro.dci.sra346servicios.model.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ProfesorDao extends CrudRepository<Profesor, Long> {
    Profesor findByUsuario(Usuario usuario);
}
