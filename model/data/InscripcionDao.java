package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.Asignatura;
import cl.ufro.dci.sra346servicios.model.Inscripcion;
import cl.ufro.dci.sra346servicios.model.Matricula;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface InscripcionDao extends CrudRepository<Inscripcion, Long> {

    Iterable<Inscripcion> findByAsignatura(Asignatura asignatura);

    Inscripcion findByMatricula(Matricula matricula);

    Inscripcion findTopByAsignaturaAndMatricula_Estudiante_estRut(Asignatura asignatura,Long est_rut);

}
