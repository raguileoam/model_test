package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.TokenRestauracion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface TokenRestauracionDao extends CrudRepository<TokenRestauracion, String> {
    TokenRestauracion findByTokTokenAndTokUsado(String token, Boolean estado);

    TokenRestauracion findByTokTokenAndTokUsadoAndTokValido(String token, Boolean estado, Boolean valido);
}
