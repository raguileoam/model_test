package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.Asistencia;
import cl.ufro.dci.sra346servicios.model.Clase;
import cl.ufro.dci.sra346servicios.model.Inscripcion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface AsistenciaDao extends CrudRepository<Asistencia, Long> {

    Asistencia findByInscripcionAndClase(Inscripcion inscripcion, Clase clase);

    Iterable<Asistencia> findByInscripcion(Inscripcion inscripcion);

    // public void update(AsistenciaEstado aes);

    @Query(value = "select a.* from asistencia a join inscripcion i on a.ins_id = i.ins_id" +
            " join matricula m on i.mat_id = m.mat_id" +
            " join asignatura asg on i.asg_id = asg.asg_id" +
            " where m.mat_numero = ?1 and asg.asg_id = ?2"
            , nativeQuery = true)
    List<Asistencia> buscarPorMatriculaYIdAsignatura(String numMatricula, Long idAsignatura);


}
