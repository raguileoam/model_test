package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.DetalleAsignatura;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface DetalleAsignaturaDao extends CrudRepository<DetalleAsignatura, Long> {
}
