package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.Clase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;

@Repository
@Transactional
public interface ClaseDao extends CrudRepository<Clase, Long> {
    Date findByclaFecha(Date claFecha);
}
