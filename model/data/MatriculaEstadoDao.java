package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.MatriculaEstado;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface MatriculaEstadoDao extends CrudRepository<MatriculaEstado, Long> {
}
