package cl.ufro.dci.sra346servicios.model.data;

import cl.ufro.dci.sra346servicios.model.TipoRol;
import org.springframework.data.repository.CrudRepository;

public interface TipoRolDao extends CrudRepository<TipoRol,Long> {
}
