package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Inscripcion {

    @Id
    @GeneratedValue
    private Long insId;

    @ManyToOne
    @JoinColumn(name = "asg_id")
    private Asignatura asignatura;

    @ManyToOne
    @JoinColumn(name = "mat_id")
    private Matricula matricula;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ins_id")
    private List<Asistencia> asistencias = new ArrayList<>();

    public Inscripcion() {
    }

    public Long getInsId() {
        return insId;
    }

    public void setInsId(Long insId) {
        this.insId = insId;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    public Matricula getMatricula() {
        return matricula;
    }

    public void setMatricula(Matricula matricula) {
        this.matricula = matricula;
    }

    public List<Asistencia> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<Asistencia> asistencias) {
        this.asistencias = asistencias;
    }

    public boolean esIdAsignatura(long id) {
        if (id == asignatura.getAsgId()) {
            return true;
        } else {
            return false;
        }
    }

    public Inscripcion(Asignatura asignatura, Matricula matricula) {
        this.asignatura = asignatura;
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "Inscripcion{" + "id=" + insId + ", asignatura=" + asignatura + ", matricula=" + matricula +

                '}';
    }
}