package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class AsistenciaEstado {

    @Id
    @GeneratedValue
    private Long aesId;
    private String aesNombre;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "aes_id")
    private List<Asistencia> asistencias = new ArrayList<>();

    public AsistenciaEstado() {
    }

    public Long getAesId() {
        return aesId;
    }

    public void setAesId(Long aesId) {
        this.aesId = aesId;
    }

    public String getAesNombre() {
        return aesNombre;
    }

    public void setAesNombre(String aesNombre) {
        this.aesNombre = aesNombre;
    }

    public List<Asistencia> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<Asistencia> asistencias) {
        this.asistencias = asistencias;
    }

    public static final String PRESENTE = "PRESENTE";
    public static final String AUSENTE = "AUSENTE";
    public static final String ATRASADO = "ATRASADO";

    public AsistenciaEstado(String nombre) {
        this.aesNombre = nombre;
    }
}