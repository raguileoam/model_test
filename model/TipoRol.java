package cl.ufro.dci.sra346servicios.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TipoRol {

    @Id
    @GeneratedValue
    private Long tprId;

    private String tprValor;

    public TipoRol() {
    }

    public TipoRol(String tprValor){
        this.tprValor = tprValor;
    }

    public Long getTprId() {
        return tprId;
    }

    public void setTprId(Long tprId) {
        this.tprId = tprId;
    }

    public String getTprValor() {
        return tprValor;
    }

    public void setTprValor(String tprValor) {
        this.tprValor = tprValor;
    }
}
