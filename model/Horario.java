package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;

@Entity
public class Horario {

    @Id
    @GeneratedValue
    private Long horId;
    private String horDia;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "per_id")
    private Periodo periodo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "das_id")
    private DetalleAsignatura detalle;

    public Horario() {
    }

    public Long getHorId() {
        return horId;
    }

    public void setHorId(Long horId) {
        this.horId = horId;
    }

    public String getHorDia() {
        return horDia;
    }

    public void setHorDia(String horDia) {
        this.horDia = horDia;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public DetalleAsignatura getDetalle() {
        return detalle;
    }

    public void setDetalle(DetalleAsignatura detalle) {
        this.detalle = detalle;
    }
}