package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;

@Entity
public class Rol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long rolId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rol_id", referencedColumnName = "usu_id")
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "tpr_id")
    private TipoRol tipoRol;

    public Rol() {
    }

    public Rol(TipoRol tipoRol, Usuario usuario){
        this.tipoRol=tipoRol;
        this.usuario=usuario;
    }

    public Rol(TipoRol tipoRol){
        this.tipoRol=tipoRol;
    }

    public Long getRolId() {
        return rolId;
    }

    public void setRolId(Long rolId) {
        this.rolId = rolId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoRol getTipoRol() {
        return tipoRol;
    }

    public void setTipoRol(TipoRol tipoRol) {
        this.tipoRol = tipoRol;
    }
}
