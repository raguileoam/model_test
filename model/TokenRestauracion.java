package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
public class TokenRestauracion {

    private static final Integer DURACION = 5 * 60 * 1000;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tokId;

    private String tokToken;

    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "usu_id")
    private Usuario usuario;

    private Date tokCreacion;

    private Boolean tokUsado;

    private Boolean tokValido;

    public TokenRestauracion() {
    }

    public TokenRestauracion(Usuario usuario) {
        this.usuario = usuario;
        this.tokCreacion = new Date();
        this.tokToken = UUID.randomUUID().toString();
        this.tokUsado = false;
        this.tokValido = true;
    }

    public boolean estaExpirado() {
        return (this.tokCreacion.getTime() + DURACION) < new Date().getTime();
    }

    public Integer getDURACION() {
        return DURACION;
    }

    public Long getTokId() {
        return tokId;
    }

    public void setTokId(Long tokId) {
        this.tokId = tokId;
    }

    public String getTokToken() {
        return tokToken;
    }

    public void setTokToken(String tokToken) {
        this.tokToken = tokToken;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getTokCreacion() {
        return tokCreacion;
    }

    public void setTokCreacion(Date tokCreacion) {
        this.tokCreacion = tokCreacion;
    }

    public Boolean getTokUsado() {
        return tokUsado;
    }

    public void setTokUsado(Boolean tokUsado) {
        this.tokUsado = tokUsado;
    }

    public Boolean getTokValido() {
        return tokValido;
    }

    public void setTokValido(Boolean tokValido) {
        this.tokValido = tokValido;
    }
}
