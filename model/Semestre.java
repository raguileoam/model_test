package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Semestre {

    @Id
    @GeneratedValue
    private Long semId;
    private String semNombre;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "sem_id")
    private List<Asignatura> asignaturas = new ArrayList<>();

    public Semestre() {
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public String getSemNombre() {
        return semNombre;
    }

    public void setSemNombre(String semNombre) {
        this.semNombre = semNombre;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }

    public Semestre(String nombre) {
        this.semNombre = nombre;
    }
}