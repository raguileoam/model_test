package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Matricula {

    @Id
    @GeneratedValue
    private Long matId;
    private String matNumero;

    @ManyToOne
    @JoinColumn(name = "est_rut")
    private Estudiante estudiante;

    @ManyToOne
    @JoinColumn(name = "mae_id")
    private MatriculaEstado matriculaEstado;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "mat_id")
    private List<Inscripcion> inscripciones = new ArrayList<>();

    public Matricula() {
    }

    public Long getMatId() {
        return matId;
    }

    public void setMatId(Long matId) {
        this.matId = matId;
    }

    public String getMatNumero() {
        return matNumero;
    }

    public void setMatNumero(String matNumero) {
        this.matNumero = matNumero;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public MatriculaEstado getMatriculaEstado() {
        return matriculaEstado;
    }

    public void setMatriculaEstado(MatriculaEstado matriculaEstado) {
        this.matriculaEstado = matriculaEstado;
    }

    public List<Inscripcion> getInscripciones() {
        return inscripciones;
    }

    public void setInscripciones(List<Inscripcion> inscripciones) {
        this.inscripciones = inscripciones;
    }

    public Matricula(String numero, Estudiante estudiante, MatriculaEstado mae) {
        this.matriculaEstado = mae;
        this.matNumero = numero;
        this.estudiante = estudiante;

    }

    @Override
    public String toString() {
        return "Matricula{" +
                "id=" + matId +
                ", numero='" + matNumero + '\'' +
                ", estudiante=" + estudiante +

                '}';
    }
}