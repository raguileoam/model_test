package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MatriculaEstado {

    @Id
    @GeneratedValue
    private Long maeId;
    private String maeEstado;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "mae_id")
    private List<Matricula> matriculas = new ArrayList<>();

    public MatriculaEstado() {
    }

    public Long getMaeId() {
        return maeId;
    }

    public void setMaeId(Long maeId) {
        this.maeId = maeId;
    }

    public String getMaeEstado() {
        return maeEstado;
    }

    public void setMaeEstado(String maeEstado) {
        this.maeEstado = maeEstado;
    }

    public List<Matricula> getMatriculas() {
        return matriculas;
    }

    public void setMatriculas(List<Matricula> matriculas) {
        this.matriculas = matriculas;
    }

    public static final String ACTIVA = "ACTIVA";
    public static final String ELIMINADA = "ELIMINADA";

    public MatriculaEstado(String estado) {
        this.maeEstado = estado;
    }
}