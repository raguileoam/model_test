package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Profesor {

    @Id
    private Long proRut;
    private String proDv;
    private String proNombres;
    private String proApellidos;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pro_rut")
    private List<Asignatura> asignaturas = new ArrayList<>();

    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "usu_id")
    private Usuario usuario;


    public Profesor() {
        this.usuario = new Usuario();
        this.asignaturas = new ArrayList<>();
    }

    public Long getProRut() {
        return proRut;
    }

    public void setProRut(Long proRut) {
        this.proRut = proRut;
    }

    public String getProDv() {
        return proDv;
    }

    public void setProDv(String proDv) {
        this.proDv = proDv;
    }

    public String getProNombres() {
        return proNombres;
    }

    public void setProNombres(String proNombres) {
        this.proNombres = proNombres;
    }

    public String getProApellidos() {
        return proApellidos;
    }

    public void setProApellidos(String proApellidos) {
        this.proApellidos = proApellidos;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Profesor(Long proRut, String proApellidos, String proDv, String proNombres, Usuario usuario) {
        this.proRut = proRut;
        this.proApellidos = proApellidos;
        this.proDv = proDv;
        this.proNombres = proNombres;
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "proRut=" + proRut +
                '}';
    }
}
