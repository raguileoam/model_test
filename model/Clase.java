package cl.ufro.dci.sra346servicios.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Clase {

    @Id
    @GeneratedValue
    private Long claId;
    private Date claFecha;
    private int claNumero;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "asg_id")
    private Asignatura asignatura;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "cla_id")
    private List<Asistencia> asistencias = new ArrayList<>();

    public Clase() {
    }

    public Clase(Asignatura asignatura, Date fecha) {
        this.asignatura = asignatura;
        this.claFecha = fecha;
    }


    public Long getClaId() {
        return claId;
    }

    public void setClaId(Long claId) {
        this.claId = claId;
    }

    public Date getClaFecha() {
        return claFecha;
    }

    public void setClaFecha(Date claFecha) {
        this.claFecha = claFecha;
    }

    public int getClaNumero() {
        return claNumero;
    }

    public void setClaNumero(int claNumero) {
        this.claNumero = claNumero;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    public List<Asistencia> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<Asistencia> asistencias) {
        this.asistencias = asistencias;
    }


    @Override
    public String toString() {
        return "Clase{" +
                "id=" + claId +
                ", fecha=" + claFecha +
                '}';
    }
}